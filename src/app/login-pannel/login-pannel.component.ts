import { Component, OnInit } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { PostService, GetService } from '../service/shared/index';
import {ItemDataService} from '../service/shared/item-data.service'
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-login-pannel',
  templateUrl: './login-pannel.component.html',
  styleUrls: ['./login-pannel.component.css']
})
export class LoginPannelComponent implements OnInit {

  public admin: any = {};
  public user: any = [];
 

  constructor(private itemDataService:ItemDataService , private router:Router,private postService:PostService,private getService:GetService) { }

  
  ngOnInit() {
    
  }


  

  login() {
    //debugger
    this.admin;
  //console.log(this.admin);
    this.postService.doLogin(this.admin).map((response) => response.json()).subscribe(
      data => {
        this.user =  data;
      //  console.log("HI ******************", JSON.stringify(data));
      //  console.log("Hello ", this.user);
       // console.log("Hello ", this.user[0].gsno);
        //console.log("Hello ", this.user[0].dob);
      //console.log("Hello ", this.user[0].name);
        
          if (this.user != null) {
            this.itemDataService.setUser(this.user);
            this.router.navigate(['/success'])    
          } else {
            this.router.navigate([''])    
          }
        
        
      },
      error => {
       // console.log("Error in Login : ", JSON.stringify(error))
        this.router.navigate(['/error'])
        setTimeout((router: Router) => {
          this.router.navigate(['/user']);
      }, 4000);  //4s
      }

    )

  }

}
