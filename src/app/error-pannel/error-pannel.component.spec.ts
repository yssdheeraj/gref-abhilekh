import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorPannelComponent } from './error-pannel.component';

describe('ErrorPannelComponent', () => {
  let component: ErrorPannelComponent;
  let fixture: ComponentFixture<ErrorPannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorPannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorPannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
