import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { UploadFileService } from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-upload-doc',
  templateUrl: './upload-doc.component.html',
  styleUrls: ['./upload-doc.component.css']
})
export class UploadDocComponent implements OnInit {

  private txtFile: any;

  public fileUploadReport: any = [];
  public uploadFileType1: any;
  public uploadFileType2: any;
  public isTextFile: boolean;
  public errorTXTFileMessage: any;
  public errorEXFileMessage: any;
  public errorTXTFileMessage1: any;
  public errorEXFileMessage2: any;
  public isExcelFile: boolean;
  public file: any = {};
  public fileupload: boolean = false;
  private size: any;
  private picSize: any;

  public documentSize: boolean = false;
  public documentType: boolean = false;
  public documentRequired: boolean = false;

  public salaryPDFFileMsg:boolean=false;
  public form16PDFFileMsg:boolean=false;
  public policyPDFFileMsg:boolean=false;
  public coursePDFFileMsg:boolean=false;

  currentFileUpload: File

  progress: { percentage: number } = { percentage: 0 }


  salaryPDFFile: FileList
  form16PdfFile: FileList
  policyPdfFile: FileList
  coursePdfFile: FileList


  result: String = "";
  filelist: any = [];
  multiplePDFFile: any = []
  filesToUpload: Array<File> = [];

  public admin: any = {}

  public user: any = {
  }
  public loginId: any = {};

  constructor(private modalService: NgbModal,private uploadFileService: UploadFileService, private getService: GetService, private postService: PostService, private http: Http,
    private itemDataService: ItemDataService) { }

  ngOnInit() {
    this.loginId = this.itemDataService.getUser();
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }



  selectFiles(event: any) {
    if (event.target.files.length > 0) {
      var selectedMutipleFile = event.target.files;
      this.result = 'Number of Files : ' + selectedMutipleFile.length;
      console.log(this.result)

      for (var i = 0; i < event.target.files.length; i++) {

        this.result += 'File Name' + selectedMutipleFile[i].name;
        this.multiplePDFFile = this.result += '';
        console.log(this.multiplePDFFile);
      }
    }
  }


  fileChangeEvent(fileInput: any) {
    if (fileInput.target.files.length > 0) {
      this.filesToUpload = <Array<File>>fileInput.target.files;
      //this.product.photo = fileInput.target.files[0]['name'];
    }
  }

  // upload() {
  //   const formData: any = new FormData();
  //   const files: Array<File> = this.filesToUpload;
  //   console.log("File List1 : ", files);
  //   this.admin.multiplePDFFile = files;
  //   console.log("File List2 : ", this.admin.multiplePDFFile);

  //   this.uploadFileService.pushMultipleFileToStorage(this.admin.multiplePDFFile).map(response => response).subscribe(event => {
  //     if (event.type === HttpEventType.UploadProgress) {
  //       this.progress.percentage = Math.round(100 * event.loaded / event.total);
  //     } else if (event instanceof HttpResponse) {
  //       console.log('Multiple Salary File is completely uploaded!');
  //     }
  //   })
  // }


  upload() {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    console.log(files);

    for (let i = 0; i < files.length; i++) {
      formData.append("uploads[]", files[i], files[i]['name']);
    }
    console.log('form data variable :   ' + formData.toString());
    this.http.post('http://localhost:7000/upload/multiple/salary/file', formData)
      //.map(files => files.json())

      .subscribe(files => console.log('files*********', files))
  }




  // documentChange(event) {
  //   debugger
  //   this.documentSize = false;
  //   this.documentType = false;
  //   this.documentRequired = false;
  //   let fileList: FileList = event.target.files;
  //   if (fileList.length > 0) {
  //     this.documentRequired = true;
  //     let file: File = fileList[0];
  //     let fileType = file.type.split('/')[1];
  //     let matchType = '|pdf|'.indexOf(fileType) >= 1;

  //     if (matchType) {
  //       this.admin.salaryPDFFile = file;
  //     }
  //     else {
  //       return this.documentType = true;
  //     }
  //   }
  // }


  selectFile(event) {
    if (event.target.files.length > 0) {
      this.salaryPDFFile = event.target.files;
      this.form16PdfFile = event.target.files;
      this.policyPdfFile = event.target.files;
      this.coursePdfFile = event.target.files;
    }

  }

  uploadDocument(value) {

    this.progress.percentage = 0;

    if (value == "1") {
      this.currentFileUpload = this.salaryPDFFile.item(0)
      this.admin.gsno = this.admin.gsno1;
      this.uploadFileService.pushFileToStorage(this.currentFileUpload, this.loginId.loginId, this.admin.gsno, this.admin.date).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.salaryPDFFileMsg = true;
          console.log('Salary File is completely uploaded!');
          this.admin.salaryPDFFile = "";
          this.admin.gsno1 = "";
          this.admin.date = "";

        }
        setTimeout(() => {
          this.progress.percentage = 0;
        }, 2000);
        setTimeout(() => {
          this.salaryPDFFileMsg = false;
        }, 3000);
      })
      this.salaryPDFFile = undefined
    }

    else if (value == "2") {
      this.currentFileUpload = this.form16PdfFile.item(0)
      this.admin.gsno = this.admin.gsno2;
      this.uploadFileService.pushForm16FileToStorage(this.currentFileUpload, this.loginId.loginId, this.admin.gsno, this.admin.from, this.admin.to).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.form16PDFFileMsg=true;
          console.log('Form16 File is completely uploaded!');
          this.admin.form16PdfFile = ""
          this.admin.gsno2 = "";
          this.admin.from = "";
          this.admin.to = "";

        }
        setTimeout(() => {
          this.progress.percentage = 0;
        }, 2000);
        setTimeout(() => {
          this.form16PDFFileMsg = false;
        }, 3000);
      })
      this.form16PdfFile = undefined
    }

    else if (value == "3") {
      this.currentFileUpload = this.policyPdfFile.item(0)
      this.uploadFileService.pushPolicyFileToStorage(this.currentFileUpload, this.loginId.loginId, this.admin.policyType).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.policyPDFFileMsg = true;
          console.log('Policy File is completely uploaded!');
          this.admin.policyPdfFile = ""
          this.admin.policyType = 0;

        }
        setTimeout(() => {
          this.progress.percentage = 0;
        }, 2000);
        setTimeout(() => {
          this.policyPDFFileMsg = false;
        }, 3000);
      })
      this.policyPdfFile = undefined
    }

    else if (value == "4") {
      this.currentFileUpload = this.coursePdfFile.item(0)
      this.uploadFileService.pushCourseFileToStorage(this.currentFileUpload, this.loginId.loginId, this.admin.courseType).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.coursePDFFileMsg=true;
          console.log('Course File is completely uploaded!');
          this.admin.coursePdfFile = ""
          this.admin.courseType = 0;

        }
        setTimeout(() => {
          this.progress.percentage = 0;
        }, 2000);
        setTimeout(() => {
          this.coursePDFFileMsg = false;
        }, 3000);
      })
      this.coursePdfFile = undefined
    }

  }

  reset(value) {
    this.progress.percentage = 0;
    if (value == 1) {
      this.admin.salaryPDFFile = "";
      this.admin.gsno1 = "";
      this.admin.date = "";
    } else if (value == 2) {
      this.admin.form16PdfFile = ""
      this.admin.gsno2 = "";
      this.admin.from = "";
      this.admin.to = "";

    }
    else if (value == 3) {
      this.admin.policyPdfFile = ""
      this.admin.policyType = 0;

    } else if (value == 4) {
      this.admin.coursePdfFile = ""
      this.admin.courseType = 0;
    }
  }


  // uploadDocument() {
  //   this.progress.percentage = 0;

  //   this.currentFileUpload = this.selectedFiles.item(0)
  //   this.uploadFileService.pushFileToStorage(this.currentFileUpload,this.loginId.loginId, this.admin.gsno, this.admin.date).subscribe(event => {
  //     if (event.type === HttpEventType.UploadProgress) {
  //       this.progress.percentage = Math.round(100 * event.loaded / event.total);
  //     } else if (event instanceof HttpResponse) {
  //       console.log('File is completely uploaded!');
  //     }
  //   })

  //   this.selectedFiles = undefined
  // }


  onPDFFileChange(event) {
    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      if (fileList && event.target.files[0]) {
        let file: File = fileList[0];
        console.log(file);

        if (file.type == "application/pdf") {
          // alert("it is pdf file")
          this.user.salaryPDFFile = file;
        } else {
          // alert("it is not a pdf file")
        }
      } else {

      }
    } else {

    }
  }



}
