import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { UploadFileService } from '../../service/shared/index'
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-posting-pannel',
  templateUrl: './posting-pannel.component.html',
  styleUrls: ['./posting-pannel.component.css']
})
export class PostingPannelComponent implements OnInit {

  public loginId: any = {};
  public user: any = {};
  public postingList: any = [];
  public noNewsCreated: boolean = false;
  numberOfData: number;
  public postings: any = {};
  public showaddpostingpage: boolean = false;
  public successmsg: boolean;
  public errormsg: boolean;
  public loading = false;
  public authenticationError: boolean;
  public deleteField: boolean;
  public updateField: boolean;
  public nodataFound: boolean = false;
  public excelErrormsg: boolean;
  myModel: any;

  public isExcelFile: boolean;
  public errorEXFileMessage: any;
  public uploadFileType: any;
  public excelfileupload: boolean = false;
  postingExcelFile: FileList
  currentFileUpload: File

  // userForm: FormGroup;
  // gsno: FormControl;
  // from_unit: FormControl;
  // to_unit: FormControl;
  // posting_order_no: FormControl;
  // nrs: FormControl;
  // movedate:FormControl;

  progress: { percentage: number } = { percentage: 0 }

  constructor(
    private modalService: NgbModal,
    private uploadFileService: UploadFileService,
    private router: Router,
    private getService: GetService,
    private postService: PostService,
    private modelService: NgbModal,
    private itemDataServiceService: ItemDataService,
    private app: AppComponent
  ) { }

  @ViewChild(NgForm) userForm: NgForm;

  ngOnInit() {
    this.getPostingList();
    this.loginId = this.itemDataServiceService.getUser();

  }

  getPostingList() {
    // if (this.postingList.length <= 0) {
    //   this.loading = true;
    // }
    this.getService.getPostingDetails().subscribe(data => {
      if (data == null) {
        this.nodataFound = true;
      } else {
        if (data.length <= 0) {
          this.loading = false;
        } else {
          if (data.length > 0) {
            this.loading = false;
            this.postingList = data;
            this.numberOfData = this.postingList.length;
          }
        }

      }
    }, error => {
    })
  }

  onXlsFileChange(event) {

    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      if (fileList && event.target.files[0]) {
        let file: File = fileList[0];

        //  alert("this is excel file");
        this.user.postingExcelFile = file;

        if ((file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (file.type == "application/vnd.ms-excel")) {
          ////console.log(file.type);
          // alert("this is text file");
          this.user.postingExcelFile = file;
          this.isExcelFile = true;

        }
        else {
          this.isExcelFile = false;
          this.errorEXFileMessage = "only .xlsx or .xls file allow";
          //alert("please fill right file");
          this.user.xlsPostingFile = "";
          this.excelErrormsg = false;
        }

      } else {
        this.isExcelFile = false;
        this.errorEXFileMessage = "only .xlsx or .xls file allow";
        this.user.xlsPostingFile = "";
        this.excelErrormsg = false;


      }



    }
  }


  uploadxlsPostingDocument() {

    if (this.isExcelFile == true) {
      this.currentFileUpload = this.user.postingExcelFile;
      this.uploadFileService.uploadExcelDocument(this.currentFileUpload, this.loginId.loginId).subscribe(event => {

       // console.log("***********1", event);
        //console.log("***********2", event.type);

        this.loading = true;
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.excelfileupload = true;
          console.log('Posting Details Excel File is completely uploaded!');
          this.user.xlsPostingFile = "";
          this.loading = false;
        }
        setTimeout(() => {
          this.progress.percentage = 0;
        }, 2000);
        setTimeout(() => {
          this.excelfileupload = false;
        }, 3000);
      },
       err => {
        this.loading = false;
        this.authenticationError=true;
        //...
    });


    } else {
      this.isExcelFile = false;
      this.errorEXFileMessage = "please upload  .xlsx or .xls file";
    }
  }


  // uploadxlsPostingDocument1() {

  //   if ((this.isExcelFile == true)) {
  //     this.uploadFileService.uploadExcelDocument(this.user.xlsPostingFile, this.loginId.loginId)
  //       .map((response) => response)
  //       .subscribe(data => {
  //         if (data.status == 200) {
  //           this.excelfileupload = true;

  //           this.user.xlsFile = "";
  //           this.user.txtFile = "";
  //           this.authenticationError = false;
  //         }

  //         setTimeout(() => {
  //           this.excelfileupload = false;

  //         }, 2000);
  //       },
  //         error => {
  //           setTimeout(() => {
  //             this.excelfileupload = false;
  //             this.authenticationError = true;

  //           }, 2000);


  //           setTimeout(() => {
  //             this.authenticationError = false;
  //           }, 2000);

  //         });
  //   }
  //   else {
  //     this.isExcelFile = false;
  //     this.errorEXFileMessage = "please upload  .xlsx or .xls file";
  //   }

  // }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }


  reset(value) {
    if (value == 1) {
      this.user.xlsPostingFile = "";
    }
  }

  showaddposting() {
    this.showaddpostingpage = true;
  }

  backpostingDetailsList() {
    this.showaddpostingpage = false;
  }




  savePostingDetails() {
    //console.log(this.user)
    this.user.createdBy = this.loginId.loginId;


    this.postService.savePostingDetails(this.user).map((response) => response)
      .subscribe(data => {
        //this.getuserList();
        this.userForm.resetForm();
        this.successmsg = true;
        setTimeout(() => {
          this.successmsg = false;
        }, 2000);

      }, error => {
        ////console.log("error ", error);
        //this.user = this.emptyUser;
        setTimeout(() => {
          // this.spinner.hide();
          this.errormsg = true;
        }, 2000)
        setTimeout(() => {
          this.errormsg = false;
        }, 4000)
      })

  }


  open(postings, deleteModal) {
    this.postings = postings;
    this.myModel = this.modelService.open(deleteModal, { windowClass: 'custom-class ' });
  }

  deletePosting(postings) {
    //debugger
    this.getService.removePostingDetails(postings.id).map((response) => response).subscribe(
      data => {
        ////console.log(data);
        setTimeout(() => {
          this, this.closeModel();
        }, 500);
        setTimeout(() => {
          this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);
        this.getPostingList();
        setTimeout(() => {
          this.closeModel();
          this.deleteField = false;
        }, 4000);
      },
      error => {
        setTimeout(() => {
          this.closeModel();
          this.deleteField = false;
          this.authenticationError = true;
        }, 2000);
        setTimeout(() => {
          this.authenticationError = false;
        }, 4000);
      });
  }


  OpenModel(postings, updateModal) {
    this.postings = postings;
    this.myModel = this.modelService.open(updateModal, { windowClass: 'custom-class' });
  }

  updatePostingDetail() {
    //debugger
    this.postService.updatePostingDetails(this.postings).map((response) => response).subscribe(
      data => {
        setTimeout(() => {
          this, this.closeModel();
        }, 500);
        setTimeout(() => {
          this.closeModel();
          this.updateField = true;
          this.authenticationError = false;
        }, 500);
        this.closeModel();
        this.getPostingList();
        setTimeout(() => {
          this.closeModel();
          this.updateField = false;
        }, 4000);
      }
    )

  }

  closeModel() {
    this.myModel.close();
  }


}
