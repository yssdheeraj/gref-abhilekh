import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostingPannelComponent } from './posting-pannel.component';

describe('PostingPannelComponent', () => {
  let component: PostingPannelComponent;
  let fixture: ComponentFixture<PostingPannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostingPannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostingPannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
