import { Component, OnInit ,ViewChild} from '@angular/core';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import {NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-news-pannel',
  templateUrl: './news-pannel.component.html',
  styleUrls: ['./news-pannel.component.css']
})
export class NewsPannelComponent implements OnInit {

  // public admin:any =[
  //   {
  //     message:''
  //   }
  // ]

  public admin:any={};
  @ViewChild('myForm') form: any;
  public loginId:any={};
  public newsList:any;
  public noNewsCreated:boolean=false;
  numberOfData: number;
  //public newsTitle:any={};
  public news:any={};
  myModel:any;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public deleteField: boolean;
  public showaddnewspage:boolean =false;


  constructor(
    private router: Router,
    private getService:GetService,
    private postService:PostService,
    private modelService:NgbModal,
    private itemDataServiceService:ItemDataService,
    private app:AppComponent
  ) { }


  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.getNewsList();
  }

  getNewsList() {
    //this.app.checkCredential();
    //debugger
    this.getService.getNewsList().subscribe(
      data =>{
       // console.log("*************" , data);
        this.newsList = data;
        this.noNewsCreated=false;
        if(this.newsList<1)
        {
          this.noNewsCreated=true;
        }
        else{
        this.numberOfData = this.newsList.length;
        this.noNewsCreated=false;
      }
      }
    )
  }

  showaddnews(){
    this.showaddnewspage = true;
  }


  backnewsList(){
    this.showaddnewspage = false;
  }

  addNews() {
    //debugger;
    let newValue = {
      message:''
    }
    this.admin.push(newValue);
  }

  

  removeNews(index:any){
    //debugger;
    this.admin.splice(index,1);
  }

  saveNews(){
    //debugger
    //this.spinner.show();
    this.admin;
    this.admin.loginId = this.loginId.loginId;
    ////console.log("******************" ,this.admin);
    this.postService.saveLatestNews(this.admin).map((response) => response).subscribe(
      data =>{
        
        this.getNewsList();
        
        if (data.status == 200) {
         // this.spinner.hide();
          this.successField = true;
          this.authenticationError = false;
        }
        setTimeout(() => {
          this.successField = false;
        }, 2000);
      },
        error => {
          setTimeout(() => {
           // this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;
          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }
     
  

  open(news,deleteModal){
    this.news = news;
    this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
  }
 
  deleteNews(news){
    //debugger
    this.getService.deleteNews(news.id).map((response) => response).subscribe(
      data => {
        ////console.log(data);
        setTimeout(() => {
          this,this.closeModel();
        },500);
        setTimeout(() => {
         this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);
        this.getNewsList();
        setTimeout(() => {
          this.closeModel();
          this.deleteField = false;
        }, 4000);
      },
        error => {
          setTimeout(() => {
          this.closeModel();
            this.deleteField = false;
            this.authenticationError = true;
          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }

  OpenModel(news,updateModal){
    this.news = news;
    this.myModel=this.modelService.open(updateModal , { windowClass: 'custom-class' });
  }


  closeModel(){
    this.myModel.close();
  }


  updateNews(){
    //debugger
    this.postService.updateNews(this.news).map((response) => response).subscribe(
      data => {
        this.closeModel();
        this.getNewsList();
      }
    )

  }
}
