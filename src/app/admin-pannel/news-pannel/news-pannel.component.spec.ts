import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsPannelComponent } from './news-pannel.component';

describe('NewsPannelComponent', () => {
  let component: NewsPannelComponent;
  let fixture: ComponentFixture<NewsPannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsPannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsPannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
