import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public loginId:any={};
  public addAdminView:boolean;

  constructor(private router: Router, private postService: PostService,
    private itmService: ItemDataService, private getService: GetService
  ) { }

  ngOnInit() {
    this.loginId = this.itmService.getUser();
    //console.log(this.loginId);
    this.checkUserType(this.loginId.userType);

  }

  logout(){
    //alert("hello logout")
    localStorage.clear();
    if (localStorage.length == 0) {
        this.router.navigate(['/admin']);
    } else {
      alert("ERROR IN LOGOUT")
      this.router.navigate(['/admin/home']);
    }
  }

  checkUserType(userType){
    //debugger
    switch(userType){
      case "superadmin":
      this.addAdminView=true;
      break;

      case "admin":
      this.addAdminView=false;
      break;
    
    }

  }
}
