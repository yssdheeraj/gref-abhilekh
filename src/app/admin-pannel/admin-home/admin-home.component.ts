import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {


  public admin: any = {};
  public userdata: any = [];
  public loading = false;
  public noOfuser: any;
  public loginId: any = {};
  public gsno: any;
  public personaldetails: any = [];
  public familydetails: any = [];
  public educationdetails: any = [];
  public showPersonalDetails: boolean = false;
  public showFamilyDetails: boolean = false;
  public showEducationDetails: boolean = false;
  public buttonDisabled: boolean;
  public globalmsg: boolean = false;


  constructor(private router: Router, private postService: PostService,
    private itmService: ItemDataService, private getService: GetService
  ) { }

  ngOnInit() {

    this.loginId = this.itmService.getUser();
    //console.log(this.loginId);

  }


  selectChangeHandler(event: any) {
    let selectedshift = event.target.value;
    this.gsno = selectedshift;
    this.buttonDisabled = true;
    console.log("if change in method " + selectedshift)
  }

  getPersonalInfo() {
    this.showFamilyDetails = false;
    this.showEducationDetails = false;
    this.gsno
    if (this.gsno == "") {
      this.showPersonalDetails = false;
      this.globalmsg = true;
    }
    else {
      this.showPersonalDetails = true;
      console.log("**********Personal", this.gsno)
      this.getService.getPersonalinfo(this.gsno).subscribe(data => {
        //console.log(data);
        this.personaldetails = data;
      })
    }
    setTimeout(() => {
      this.globalmsg = false;
    }, 2000);
  }

  getFamilyInfo() {

    this.showPersonalDetails = false;
    this.showEducationDetails = false;
    this.gsno
    if (this.gsno == "") {
      this.showFamilyDetails = false
      this.globalmsg = true;
    } else {
      this.showFamilyDetails = true
      //console.log("**********Family", this.gsno)
      this.getService.getFamilydetails(this.gsno).subscribe(data => {
        this.familydetails = data
        //console.log(this.familydetails);
      })
    }
    setTimeout(() => {
      this.globalmsg = false;
    }, 2000);
  }


  getEducationInfo() {

    this.showPersonalDetails = false;
    this.showFamilyDetails = false;

    this.gsno
    // console.log("**********Education", this.gsno)
    if (this.gsno == "") {
      this.showEducationDetails = false;
      this.globalmsg = true;
    } else {
      this.showEducationDetails = true;
      this.getService.getEducationdetails(this.gsno).subscribe(data => {
        // console.log(data);
        this.educationdetails = data;
        //console.log(this.educationdetails);
      })
    }
    setTimeout(() => {
      this.globalmsg = false;
    }, 2000);
  }

}
