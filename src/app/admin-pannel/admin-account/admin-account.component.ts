import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms'
import { RouterModule, Router } from '@angular/router';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  FormsModule,
  FormControl,
  ReactiveFormsModule, FormGroup, Validators
} from '@angular/forms'
import { error, Key } from 'selenium-webdriver';


@Component({
  selector: 'app-admin-account',
  templateUrl: './admin-account.component.html',
  styleUrls: ['./admin-account.component.css']
})
export class AdminAccountComponent implements OnInit {

  public selectAllView: boolean;
  public appReportView: boolean;
  public blacklistReportView: boolean;
  public documentuploadReportView: boolean;
  public successmsg:boolean;
  public errormsg:boolean
  public user: any = {};
  public loginId: any = {};

  constructor(private router: Router, private itemDataServiceService: ItemDataService, private getService: GetService, private postService: PostService,
    private modelService: NgbModal
  ) {
    this.user.userType = 0;
  }

  @ViewChild(NgForm) userForm: NgForm;
  


  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
  }

  isselectAllChecked() {
    //console.log(this.user.selectAllView)
    this.selectAll(this.user.selectAllView)
  }

  selectAll(value: boolean) {
    if (value) {
      this.user.appReportView = true;
      this.user.blacklistReportView = true;
      this.user.documentuploadReportView = true;

    } else {
      this.user.appReportView = false;
      this.user.blacklistReportView = false;
      this.user.documentuploadReportView = false;
    }
  }

  saveAccount() {
    //console.log(this.user)
    this.user.createdBy = this.loginId.loginId;
    if (this.user.password == this.user.confPassword) {

      this.postService.saveUser(this.user).map((response) => response)
        .subscribe(data => {
          //this.getuserList();
          if (data.status == 201) {

            
            ////console.log("save data" + data);
            // this.user = this.emptyUser;
            //   this.spinner.hide();
               this.successmsg = true;
               this.userForm.resetForm();
          //   this.router.navigate(['/account/create/success'])
          //   setTimeout((router: Router) => {
          //     this.router.navigate(['/admin/account']);
          // }, 3000);  //4s
            
          }
          setTimeout(() => {
           this.successmsg = false;
          }, 2000);

        }, error => {
          ////console.log("error ", error);
          //this.user = this.emptyUser;
          setTimeout(() => {
            // this.spinner.hide();
            this.errormsg = true;
          }, 2000)
          setTimeout(() => {
            this.errormsg = false;
          }, 4000)
        })
    } else {
      setTimeout(() => {
        alert("Create Password And Confirm Password Must Be Same")
        //  this.spinner.hide();
        //this.errorpassword = true;
      }, 2000)
      setTimeout(() => {
        //this.errorpassword = false;
      }, 4000)
    }
  }


  empty() {
    this.user.userName = '';
    this.user.loginId = '';
    this.user.mobile = '';
    this.user.password = '';
    this.user.emailId = '';
    this.user.confPassword = '';
  }

}
