import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import {ItemDataService} from '../../service/shared/item-data.service'
import {PostService,GetService} from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-apk-details',
  templateUrl: './apk-details.component.html',
  styleUrls: ['./apk-details.component.css']
})
export class ApkDetailsComponent implements OnInit {

  constructor(private router:Router,private postService:PostService,private getService:GetService,private itemDataService:ItemDataService,private http: Http) { }

  public downloadapkuser: any = [];
  public loginId:any={};
  numberOfData: number;
  

  ngOnInit() {
    this.getApkDetails();
  }


  getApkDetails(){
    this.getService.getDownloadApkDetails().subscribe( data=>{
      this.downloadapkuser = data;
      this.numberOfData = this.downloadapkuser.length;
     //console.log("ddddddddddddddd" , this.downloadapkuser);
    })
  }

}
