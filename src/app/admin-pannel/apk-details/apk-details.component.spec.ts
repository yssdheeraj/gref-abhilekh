import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApkDetailsComponent } from './apk-details.component';

describe('ApkDetailsComponent', () => {
  let component: ApkDetailsComponent;
  let fixture: ComponentFixture<ApkDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApkDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApkDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
