import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlacklistMobileComponent } from './blacklist-mobile.component';

describe('BlacklistMobileComponent', () => {
  let component: BlacklistMobileComponent;
  let fixture: ComponentFixture<BlacklistMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlacklistMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlacklistMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
