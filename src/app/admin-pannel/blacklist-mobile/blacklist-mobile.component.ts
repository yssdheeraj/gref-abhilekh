import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blacklist-mobile',
  templateUrl: './blacklist-mobile.component.html',
  styleUrls: ['./blacklist-mobile.component.css']
})
export class BlacklistMobileComponent implements OnInit {


  public addinBlackListmsg: boolean;
  public deletefromBlackListmsg: boolean;
  public disabled: boolean = true;
  public showAddBlackList: boolean = false;
  public showuserListForBlacklist: boolean = false;
  public noOfuser: any;
  public noofBlackList: any;
  public userList: any = []
  public selectuserList: any = []
  public userdata: any = [];
  public blacklistuser: any = []
  public loading = false;
  public p: number = 1
  public selectUserForBlackList: any = 0;
  public loginId: any = {}
  public user: any = {}
  public blackUser: any = {}
  
  public filterUser: any = []



  public isUserBlackListed: boolean = false;

  constructor(private router: Router, private postService: PostService, private getService: GetService, private itemDataService: ItemDataService) { }

  ngOnInit() {
    this.getAllUserDetails();
    this.loginId = this.itemDataService.getUser();
    this.getBlackListUser();
  }

  getAllUserDetails() {
    if(this.userdata.length<=0){
      this.loading=true;
    }
    
    this.getService.getUserDetails().subscribe(data => {
      if(data.length>0)
      {this.loading=false;
      this.userdata = data;
     // console.log(this.userdata);
      this.noOfuser = this.userdata.length;
      }
    },error=>{
    })
  }


  getBlackListUser() {
    this.getService.getBlackListUserDetails().subscribe(data => {
        this.blacklistuser = data;
        // console.log("****Black List Users****", this.blacklistuser)
        this.noofBlackList = this.blacklistuser.length;
      },
        error => {
          setTimeout(() => {

          }, 1000);

          setTimeout(() => {

          }, 4000);

        });
  }


  // fiterUserAfterBlackList() {

  //   this.blacklistuser = this.getBlackListUser();
  //   this.userdata = this.getAllUserDetails();

  //console.log("****Black List Users1****", this.blacklistuser)
  //console.log("***All UserList1*****", this.userdata);
  //let missing = this.userdata.filter(item => this.blacklistuser.indexOf(item) < 0);
  //console.log(missing);
  //}

  addBlackList() {
    this.selectuserList=[];
    this.selectUserForBlackList='';
    this.disabled = true;
    this.showAddBlackList = true;
    this.getAllUserDetails();
  }

  backToBlackList() {
    this.getAllUserDetails();
    this.getBlackListUser();
    this.selectUserForBlackList='';
    this.selectuserList=[];
    this.showAddBlackList = false;
  }

  showUserForBlackList() {
    this.selectUserForBlackList='';
    this.showuserListForBlacklist = false;
  }


  addUserForBlackList() {
    this.selectUserForBlackList='';
    this.disabled = true;
    this.showuserListForBlacklist = true;
  }


  selectUserCheck(i, userList) {
    //  debugger
    if (i.target.checked) {



      this.selectuserList.push(userList);
      //  console.log(this.selectuserList);
      this.selectUserForBlackList = this.selectuserList.length;
      // console.log(this.selectuserList.length);

    } else {
      this.selectuserList.splice(this.selectuserList, 1);
      //console.log(this.selectuserList);

      this.selectUserForBlackList = this.selectuserList.length;
      //console.log(this.selectuserList.length);

    }
    if (this.selectuserList.length > 0) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }


  addUser_In_BlackList() {
   this.loading=true;
    this.addinBlackListmsg = false;
    this.user = this.selectuserList
    this.user.blacklistby = this.loginId.loginId
    //console.log("List of BlackList Users", this.user);
    this.postService.addUserinBlackList(this.user).map((response) => response)
      .subscribe(data => {
        //  console.log(data)
        if(data.status=200)
        {
          this.disabled=true
          this.loading=false;
          this.selectUserForBlackList='';
         
         
        }
        setTimeout(() => {
          this.addinBlackListmsg = true;
        }, 200);
        this.selectuserList = [];
        this.getAllUserDetails();
        this.getBlackListUser();
        setTimeout(() => {
          this.addinBlackListmsg = false;
        }, 4000);

      },
        error => {
          setTimeout(() => {

          }, 1000);

          setTimeout(() => {

          }, 4000);

        });
  }

  selectblackUserCheck(i, blacklistuser) {
    if (i.target.checked) {

      this.selectuserList.push(blacklistuser);
    // console.log(this.selectuserList);
      this.selectUserForBlackList = this.selectuserList.length;
       //console.log(this.selectuserList.length);

    } else {
      this.selectuserList.splice(this.selectuserList, 1);
      //console.log(this.selectuserList);

      this.selectUserForBlackList = this.selectuserList.length;
      //console.log(this.selectuserList.length);

    }
    if (this.selectuserList.length > 0) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }

  deleteUser_From_BlackList() {
   this.loading=true;
    this.deletefromBlackListmsg = false;
    this.blackUser = this.selectuserList;

    this.postService.deleteUserinBlackList(this.blackUser).map((response) => response)
      .subscribe(data => {
        if(data.status=200)
        {
          this.disabled=true
          this.loading=false;
          this.selectUserForBlackList='';
        }
        setTimeout(() => {
          this.deletefromBlackListmsg = true;
        }, 200);

        this.getBlackListUser();
        this.selectuserList = [];
        setTimeout(() => {
          this.deletefromBlackListmsg = false;
        }, 4000);

      },
        error => {
          setTimeout(() => {

          }, 1000);

          setTimeout(() => {

          }, 4000);

        });

  }

  // checkUserInBlackListExist() {
  //   let ret: any[] = [];
  //   for (let i in this.userdata) {
  //     let isDup: boolean = false;
  //     for (let j in this.blacklistuser) {
  //       if (this.userdata[i].gsno == this.blacklistuser[j].gsno) {
  //         //this.userdata.splice(this.userdata.indexOf(i),1);
  //         isDup = true;
  //         break;
  //       }
  //     }
  //     if (!isDup) ret.push(i);
  //     console.log("****************************ddddddd", this.userdata)
  //   }
  //   return ret;
  // }

}
