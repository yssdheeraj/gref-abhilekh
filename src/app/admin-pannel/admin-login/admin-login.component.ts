import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from '../../service/shared/item-data.service'
import { PostService, GetService } from '../../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  public successField: boolean;
  public user: any = {};
  public loginId: any = {};
  public mobile: any = {};
  public allLoginDetails: any = [];
  public forgetpasswordPage: boolean = false;
  public loginpage: boolean = true;
  public newsList: any;
  public wrongId: boolean;
  public wrongMobile: boolean
  numberOfData: number;
  public isCredential: boolean = false;

  public admin: any = {};
  // loginId:any='';
  // password:any='';
  WIDTH: any = 800;
  HEIGHT: any = 505;

  images: any = [
    { item: 2 }, { item: 3 }, { item: 4 }, { item: 5 }, { item: 6 }, { item: 7 }, { item: 8 }
  ]

  constructor(private router: Router, private postService: PostService,
    private itmService: ItemDataService, private getService: GetService
  ) { }
  ngOnInit() {
  }

  adminLogin() {
  // debugger
    this.admin;
    //console.log(this.admin);
    //this.router.navigate(['/home'])
    this.postService.adminDoLogin(this.admin).map((response) => response.json()).subscribe(
      data => {
        this.user = data;
       //console.log(this.user);

        if (this.user != null) {
          this.itmService.setUser(this.user);
          this.router.navigate(['/admin/home'])
        }
        else {

          this.router.navigate(['/admin'])

          this.successField = false;
          //this.authenticationError = true;
          setTimeout(() => {
            //this.authenticationError = false;
          }, 5000);

        }
      },
      error => {
        // console.log("Error in Login : ", JSON.stringify(error))
         this.router.navigate(['/error'])
         setTimeout((router: Router) => {
           this.router.navigate(['/admin']);
       }, 4000);  //4s
       }
    );
  }

}



