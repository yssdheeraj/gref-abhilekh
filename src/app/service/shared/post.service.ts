import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { ItemDataService } from '../../service/shared/item-data.service'

@Injectable()
export class PostService {



  private baseUrl: string = "";
  //private url = "http://localhost:7000";

  private headers = new Headers({ 'content-type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http, private itemDataService: ItemDataService) { }

  ngOnInit() {

    this.getUrl();
  }
  getUrl() {
    this.baseUrl = this.itemDataService.getBaseurl();

  }

  //admin login
  adminDoLogin(value: any): Observable<any> {
    //debugger
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/login/admin", JSON.stringify(value), this.options);
  }


  //userlogin
  doLogin(value: any): Observable<any> {
    //  debugger
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/querydata/ulogin/api?queryid=19&gsno=" + value.gsno + "&dob=" + value.dob, JSON.stringify(value), this.options);
  }

  downloadApk(value: any): Observable<any> {
    //  debugger
    this.getUrl();
    return this.http.post(this.baseUrl + '/pannel/user/login/detail', JSON.stringify(value), this.options);

  }

  saveUser(value: any): Observable<any> {
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/create/account/admin", JSON.stringify(value), this.options);
  }

  addUserinBlackList(value: any): Observable<any> {
    // debugger
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/add/user/blacklist", JSON.stringify(value), this.options);
  }

  addUserinBlackListManually(value: any): Observable<any> {
    // debugger
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/add/user/manually/blacklist", JSON.stringify(value), this.options);
  }

  deleteUserinBlackList(value: any): Observable<any> {
    //debugger
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/delete/user/blacklist", JSON.stringify(value), this.options);
  }

  saveLatestNews(value: any): Observable<any> {
    //debugger
    this.getUrl();
    return this.http.post(this.baseUrl + '/querydata/save/event', JSON.stringify(value), this.options);
  }

  updateNews(value: any): Observable<any> {
    //debugger  
    this.getUrl();
    return this.http.put(this.baseUrl + "/querydata/update/news", JSON.stringify(value), this.options);
  }

  savePostingDetails(value: any): Observable<any> {
    this.getUrl();
    return this.http.post(this.baseUrl + "/pannel/create/posting/details", JSON.stringify(value), this.options);
  }

  updatePostingDetails(value: any): Observable<any> {
    //debugger  
    this.getUrl();
    return this.http.put(this.baseUrl + "/pannel/update/posting/details", JSON.stringify(value), this.options);
  }


}
