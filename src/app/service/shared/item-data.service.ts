import { Injectable } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@Injectable()
export class ItemDataService {

  public user: any = {};
  public key: any;
  public baseUrl: any;


  constructor(private router: Router) { }


  public setUser(user) {
    // console.log("SET USER", user)
    localStorage.setItem(this.key, JSON.stringify(user));
  }


  public getUser() {
    let user;
    ////console.log("user data item ",this.user);
    if (localStorage.length == 0) {
      this.router.navigate(['']);
    }
    return JSON.parse(localStorage.getItem(user));
  }

  public setBaseurl(baseUrl) {
    //  console.log(" set base user",baseUrl)
    this.baseUrl = baseUrl;
  }
  public getBaseurl() {
    // console.log(" set base user",this.baseUrl)
    return this.baseUrl;
  }
}


