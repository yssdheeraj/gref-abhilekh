import { Injectable } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ItemDataService } from '../../service/shared/item-data.service'


@Injectable()
export class GetService {

  private ip: any[];
  private myIP: any;
  private myPort: any;
  private myapp: any;

  private baseUrl = "";
  public loginId: any = {};

  //private url = "http://localhost:7000";


  constructor(private http: Http, private itemDataService: ItemDataService) { }

  ngOnInit() {

    this.getIp();
  }

  getIp() {
    return this.http.get("./assets/data.json").map((response) => response.json())
      .subscribe(data => {
       
        this.ip = data;
        this.myIP = this.ip[0].ip;
        this.myPort = this.ip[0].port;
        this.myapp = this.ip[0].app;
        if (this.myIP == "localhost") {
          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        } else {

          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        }
        this.itemDataService.setBaseurl(this.baseUrl);

      });
  }

  getCheckCredential() {
    this.getIp();
    this.itemDataService.getBaseurl;
  }



  getDownloadApkBygsno(value: any) {
    //debugger
    return this.http.get(this.baseUrl + "/pannel/get/apk/status?gsno=" + value).map((response) => response.json());
  }

  getDownloadApkDetails() {
    return this.http.get(this.baseUrl + '/pannel/get/downlaod/apk/details').map((response) => response.json());
  }


  getUserDetails() {
    return this.http.get(this.baseUrl + "/pannel/querydata/getUserDetails/api?queryid=25&title=user_details").map((response) => response.json());
  }

  getPersonalinfo(value:any){
   // console.log(this.baseUrl);
    return this.http.get(this.baseUrl + "/querydata/api?id=1&gsno=" +value).map((response) => response.json());
  }

  getFamilydetails(value:any){
    //console.log(this.baseUrl);
    return this.http.get(this.baseUrl + "/querydata/api?id=5&gsno=" +value).map((response) => response.json());
  }

  getEducationdetails(value:any){
    //console.log(this.baseUrl);
    return this.http.get(this.baseUrl + "/querydata/api?id=2&gsno=" +value).map((response) => response.json());
  }
  
  getBlackListUserDetails() {
    return this.http.get(this.baseUrl + '/pannel/get/blacklist/users/details').map((response) => response.json());
  }

  getNewsList(): Observable<any> {
    // debugger
    return this.http.get(this.baseUrl + "/querydata/get/event/pannel").map((response) => response.json());
  }

  deleteNews(value: any): Observable<any> {
    // debugger
    return this.http.delete(this.baseUrl + "/querydata/delete/news/" + value).map(success => success.status);
  }

  getPostingDetails(): Observable<any> {
    return this.http.get(this.baseUrl + "/pannel/get/posting/details").map((response) => response.json());
  }

  removePostingDetails(value: any): Observable<any> {
    // debugger
    return this.http.delete(this.baseUrl + "/pannel/delete/posting/" + value).map(success => success.status);
  }


}
