import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpUrlEncodingCodec } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response, RequestOptions, Headers, Jsonp } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observer } from 'rxjs/Observer';
import { domRendererFactory3 } from '@angular/core/src/render3/renderer';
import { ItemDataService } from '../../service/shared/item-data.service'


@Injectable()
export class UploadFileService {

  private baseUrl: string = "";

  // constructor(private http: HttpClient) { }
  constructor(private http: HttpClient, private itemDataService: ItemDataService) { }

  ngOnInit() {

    this.getUrl();
  }
  getUrl() {
    this.baseUrl = this.itemDataService.getBaseurl();

  }


  pushFileToStorage(file: File, loginId, gsno, date): Observable<HttpEvent<{}>> {
    // debugger
    this.getUrl();
    let formdata: FormData = new FormData();

    formdata.append('file', file);
    formdata.append('loginId', loginId);
    formdata.append('gsno', gsno);
    formdata.append('date', date);

    const req = new HttpRequest('POST', this.baseUrl + '/upload/salary/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }


  pushForm16FileToStorage(file: File, loginId, gsno, from, to): Observable<HttpEvent<{}>> {
    this.getUrl();
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('loginId', loginId);
    formdata.append('gsno', gsno);
    formdata.append('from', from);
    formdata.append('to', to);
    const req = new HttpRequest('POST', this.baseUrl + '/upload/form16/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);

  }

  pushPolicyFileToStorage(file: File, loginId, policyType): Observable<HttpEvent<{}>> {
    //debugger
    this.getUrl();
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('loginId', loginId);
    formdata.append('policyType', policyType);
    const req = new HttpRequest('POST', this.baseUrl + '/upload/policy/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);

  }


  pushCourseFileToStorage(file: File, loginId, courseType): Observable<HttpEvent<{}>> {
    //debugger
    this.getUrl();
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('loginId', loginId);
    formdata.append('courseType', courseType);
    const req = new HttpRequest('POST', this.baseUrl + '/upload/course/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);

  }

  uploadExcelDocument(file: File, loginId): Observable<HttpEvent<{}>> {
  
    this.getUrl();
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    formdata.append('loginId', loginId);
    const req = new HttpRequest('POST', this.baseUrl + '/upload/posting/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);

  }

  pushMultipleFileToStorage(value: any): Observable<HttpEvent<{}>> {
    this.getUrl();
    const files: Array<File> = value;

    let formdata: FormData = new FormData();

    for (let i = 0; i < files.length; i++) {
      formdata.append("uploads[]", files[i], files[i]['name']);
    }
    console.log('form data variable :   ' + formdata.toString());

    const req = new HttpRequest('POST', this.baseUrl + '/upload/multiple/salary/file', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

}
