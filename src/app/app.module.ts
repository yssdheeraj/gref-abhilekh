import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms'
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { RouterModule, Routes } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';

import { PostService, GetService ,UploadFileService} from './service/shared/index';
import { ItemDataService } from './service/shared/item-data.service'

import { AppComponent } from './app.component';
import { LoginPannelComponent } from './login-pannel/login-pannel.component';
import { SuccessPannelComponent } from './success-pannel/success-pannel.component';
import { ErrorPannelComponent } from './error-pannel/error-pannel.component';
import { MenuPannelComponent } from './menu-pannel/menu-pannel.component';
import { AdminLoginComponent } from './admin-pannel/admin-login/admin-login.component';
import { MenuComponent } from './admin-pannel/menu/menu.component';
import { AdminHomeComponent } from './admin-pannel/admin-home/admin-home.component';
import { UploadDocComponent } from './admin-pannel/upload-doc/upload-doc.component';
import { BlacklistMobileComponent } from './admin-pannel/blacklist-mobile/blacklist-mobile.component';
import { ApkDetailsComponent } from './admin-pannel/apk-details/apk-details.component';
import { AdminAccountComponent } from './admin-pannel/admin-account/admin-account.component';
import { SuccessComponent } from './admin-pannel/success/success.component';
import { EventComponent } from './admin-pannel/event/event.component';
import { HomeComponent } from './home/home.component';
import { NewsPannelComponent } from './admin-pannel/news-pannel/news-pannel.component';
import { PostingPannelComponent } from './admin-pannel/posting-pannel/posting-pannel.component';

const appRoutes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path:'posting', component:PostingPannelComponent
  },
  {
    path: 'admin/event', component: NewsPannelComponent
  },
  {
    path: 'account/create/success', component: SuccessComponent
  },
  {
    path: 'admin/account', component: AdminAccountComponent
  },
  {
    path: 'user', component: LoginPannelComponent
  },
  {
    path: 'admin', component: AdminLoginComponent
  },
  {
    path: 'admin/home', component: AdminHomeComponent
  },
  {
    path: 'upload/document', component: UploadDocComponent
  },
  {
    path: 'blacklist/mobile', component: BlacklistMobileComponent
  },
  {
    path: 'apk/details', component: ApkDetailsComponent
  },
  {
    path: 'success', component: SuccessPannelComponent
  },
  {
    path: 'error', component: ErrorPannelComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginPannelComponent,
    SuccessPannelComponent,
    ErrorPannelComponent,
    MenuPannelComponent,
    AdminLoginComponent,
    MenuComponent,
    AdminHomeComponent,
    UploadDocComponent,
    BlacklistMobileComponent,
    ApkDetailsComponent,
    AdminAccountComponent,
    SuccessComponent,
    EventComponent,
    HomeComponent,
    NewsPannelComponent,
    PostingPannelComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    AngularFontAwesomeModule,
    NgxPaginationModule,
    LoadingModule,
  ],
  providers: [PostService, GetService, ItemDataService,UploadFileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
