import { Component, OnInit } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import {ItemDataService} from '../service/shared/item-data.service'
import {PostService,GetService} from '../service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-success-pannel',
  templateUrl: './success-pannel.component.html',
  styleUrls: ['./success-pannel.component.css']
})
export class SuccessPannelComponent implements OnInit {

  constructor(private router:Router,private postService:PostService,private getService:GetService,private itemDataService:ItemDataService,private http: Http) { }

  public user: any = {};
  public loginId:any=[];
  
  public gsno:any;
  public apkCount:any=0;
  public download_apk:any;
  public apkDownloadMessage:boolean=false;

  ngOnInit() {
    this.loginId = this.itemDataService.getUser();
   // console.log("SUCCESS PANNEL" , this.loginId);
    this.apkstaus();
  }


  apkstaus(){
    //debugger
    this.loginId[0].gsno;
    //console.log("gsno " ,this.loginId[0].gsno);
    this.gsno = this.loginId[0].gsno;
    this.getService.getDownloadApkBygsno(this.gsno).subscribe( data=>{
      this.apkCount = data;
        //console.log("ApkCount = " , this.apkCount);  
      
    },error => {
     // console.log("Error in Apk Count : ", JSON.stringify(error))
    }
  );
  } 

  downloadFile() {
    //debugger
    //console.log(this.loginId)
    // console.log(this.loginId[0].gsno)
    // console.log(this.loginId[0].dob)
    // console.log(this.loginId[0].name)
    

    // this.user = this.loginId[0].gsno;
    // this.user = this.loginId[0].dob;
    // this.user = this.loginId[0].name;
    
   // console.log("*********" ,this.loginId[0])
    return this.http.get('assets/download_file/yss.apk', {
      responseType: ResponseContentType.Blob,
      //search: // query string if have
      })
      .map(res => {
        return {
          filename: 'yss.apk',
          data: res.blob()
        };
      })
      .subscribe(res => {
        //console.log('start download:', res);
        var url = window.URL.createObjectURL(res.data);
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = res.filename;
        a.click();
        window.URL.revokeObjectURL(url);

        this.apkDownloadMessage =  true;
        
        this.download_apk = this.apkCount+1;
        //console.log("Download APK NO OF TIME" , this.download_apk);

        this.user.gsno = this.loginId[0].gsno;
        this.user.dob = this.loginId[0].dob;
        this.user.name = this.loginId[0].name;
        this.user.mobile = this.loginId[0].mobile;
        this.user.aadhar = this.loginId[0].aadhar;
        this.user.download_apk = this.download_apk;
        //this.user = this.download_apk;
        //console.log("###########" , this.user)

        this.postService.downloadApk(this.user).map((response) => response).subscribe(data => {
          //console.log("Data" ,data);
            this.apkstaus();
        })
        setTimeout(() => {
          this. apkDownloadMessage= false;
        }, 2000);
        a.remove(); // remove the element
      }, error => {
        //console.log('download error:', JSON.stringify(error));
      }, () => {
        //console.log('Completed file download.')
          
      }
    );

 
  }

}
