import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessPannelComponent } from './success-pannel.component';

describe('SuccessPannelComponent', () => {
  let component: SuccessPannelComponent;
  let fixture: ComponentFixture<SuccessPannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessPannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessPannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
