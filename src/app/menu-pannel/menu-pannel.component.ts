import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import {ItemDataService} from '../service/shared/item-data.service'

@Component({
  selector: 'app-menu-pannel',
  templateUrl: './menu-pannel.component.html',
  styleUrls: ['./menu-pannel.component.css']
})
export class MenuPannelComponent implements OnInit {

  public loginId:any=[];

  constructor(private router:Router, private itemDataService:ItemDataService) { }

  ngOnInit() {
      this.loginId = this.itemDataService.getUser();
  }


  logoutUser(){
    //alert("hello logout")
    localStorage.clear();
    if (localStorage.length == 0) {
        this.router.navigate(['/user']);
    } else {
      alert("ERROR IN LOGOUT")
      this.router.navigate(['/success']);
    }
  }

}
