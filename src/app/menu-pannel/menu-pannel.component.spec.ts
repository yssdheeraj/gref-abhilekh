import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPannelComponent } from './menu-pannel.component';

describe('MenuPannelComponent', () => {
  let component: MenuPannelComponent;
  let fixture: ComponentFixture<MenuPannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
