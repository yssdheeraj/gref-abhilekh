import { Component } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { ItemDataService } from './service/shared/item-data.service'
import { PostService, GetService } from './service/shared/index'
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public loginId:any=[];
  public now: Date = new Date();

  constructor(private router: Router, private postService: PostService,
    private itmService: ItemDataService, private getService: GetService
  ) {
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }



  ngOnInit(){
    this.getService.getIp();
    this.loginId=this.itmService.getUser();
    this.checkCredential();
    ////console.log("user  home ---name ",this.loginId)
  }

  checkCredential(){
    this.getService.getCheckCredential()
  }
  

  
}
